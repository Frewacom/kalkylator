#include "mainwindow.h"
#include "ui_mainwindow.h"

// Att göra: Algoritm för att automatiskt lägga till och
// kolla efter olika operatorer m.m, samt göra en lista för
// olika operatorer och respektive prioriteringar
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Numbers
    connect(ui->N_0, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_1, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_2, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_3, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_4, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_5, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_6, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_7, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_8, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
    connect(ui->N_9, SIGNAL(clicked()), this, SLOT(onButtonClicked()));

    // Operators
    connect(ui->Plus, SIGNAL(clicked()), this, SLOT(onOperatorClicked()));
    connect(ui->Minus, SIGNAL(clicked()), this, SLOT(onOperatorClicked()));
    connect(ui->Divided, SIGNAL(clicked()), this, SLOT(onOperatorClicked()));
    connect(ui->Times, SIGNAL(clicked()), this, SLOT(onOperatorClicked()));

    // Enter
    connect(ui->Enter, SIGNAL(clicked()), this, SLOT(onEnterClicked()));

    // Clear
    connect(ui->Clear, SIGNAL(clicked()), this, SLOT(onClearClicked()));

    // Comma
    connect(ui->Comma, SIGNAL(clicked()), this, SLOT(onCommaClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Hämtar texten från QTextEdit
QString MainWindow::getCurrentOutputText()
{
    QString currentText = ui->Output->toPlainText();
    return currentText;
}

// Delar upp texten från QTextEdit via alla blanksteg
QStringList MainWindow::getSplitOutput()
{
    return getCurrentOutputText().split(" ");
}

// Skriver ut texten till output
void MainWindow::write(QString text)
{
    ui->Output->setText(text);
}

// När man trycker på en sifferknapp
void MainWindow::onButtonClicked()
{
    // Om ett uträknat svar står i rutan,
    // kolla ifall vi ska rensa och börja om eller
    // fortsätta bygga på svaret.
    if (answerIsDisplayed)
    {
        ui->Output->clear();
        answerIsDisplayed = false;

        // modf kollar ifall en double har decimaler
        double temp;
        if (!modf(answer, &temp) != 0)
        {
            containsComma = false;
        }
        else
        {
            containsComma = true;
        }
    }

    // Via QWidget::sender kan vi få fram nummret på knappen
    // (Alla sifferknappar heter N_X, där x är siffran);
    QString objName = QWidget::sender()->objectName();
    QCharRef number = objName[objName.length()-1];
    QString output = getCurrentOutputText();

    // Om det står en nolla i textrutan så vill vi inte lägga på t.ex
    // en etta utan att först lägga till ett comma
    if (output[0] == "0"
            && !hasOperator)
    {
        // Om det inte finns ett comma så rensar vi textrutan
        // och skriver bara ut siffran vi klickade på
        if (!containsComma)
        {
            write(QString(number));
        }
    }
    else
    {
        // Annars skriver vi ut allt som stod i rutan + nummret
        write(output + number);
    }
}

// När man klickar på plus/minus/division/multipl.
void MainWindow::onOperatorClicked()
{
    // Via QWidget::sender kan vi få fram vilket operator
    // det är (objectName == Plus, Minus, Division, Times)
    QString output = getCurrentOutputText();
    QString sender = QWidget::sender()->objectName();
    QStringList split = getSplitOutput();

    // Om objectName finns i operatorsString ("+/+-")
    if (!operators.contains(split[split.count()-1]))
        if (sender == "Plus")
        {
            output += " + ";
        }
        else if (sender == "Minus")
        {
            output += " - ";
        }
        else if (sender == "Divided")
        {
            output += " / ";
        }
        else if (sender == "Times")
        {
            output += " * ";
        }
        else
        {
            output = "maddafakka broke";
        }

        hasOperator = true;
        answerIsDisplayed = false;
        containsComma = false;

    // Skriv ut operatorn
    write(output);
}

// När man klickar på C (clear)
void MainWindow::onClearClicked()
{
    // Töm textrutan och återställ booleans
    ui->Output->clear();
    answerIsDisplayed = false;
    hasOperator = false;
}

// När man klickar på comma
void MainWindow::onCommaClicked()
{
    QString output = getCurrentOutputText();
    QStringList split = getSplitOutput();

    // Om sista termen inte innehåller en operator
    if (!operators.contains(split[split.count()-1])
            && !containsComma)
    {
        // Kolla ifall den sista termen innehåller decimaler
        double temp;
        if (modf(split[split.count()-1].toDouble(), &temp) == 0)
        {
            // Om inte, lägg till ett comma
            write(output + ".");
            containsComma = true;
        }

        // Annars skriver vi inte ut något, men säger att
        // svaret inte längre visas då den inte är densamma
        if (answerIsDisplayed)
        {
            answerIsDisplayed = false;
        }
    }
}

// Addition
double MainWindow::plus(double x, double y)
{
    double n = x + y;
    return n;
}

// Subtraktion
double MainWindow::minus(double x, double y)
{
    double n = x - y;
    return n;
}

// Division
double MainWindow::divided(double x, double y)
{
    double n = x / y;
    return n;
}

// Multiplikation
double MainWindow::times(double x, double y)
{
    double n = x * y;
    return n;
}

// Räknar ut
void MainWindow::calculate()
{
    QStringList split = getSplitOutput();
    bool timesOrDivision = false;

    // Om första och sista termen inte är plus/minus/division/multipl.
    if (!operators.contains(split[split.count()-1])
            && !operators.contains(split[0]))
    {
        // Kolla om multipl. eller division finns med
        for (int i = 0; i < split.count(); i++)
        {
            // Om det finns multipl. eller division i
            // textrutan
            if (split[i] == "*" || split[i] == "/")
            {
                timesOrDivision = true;
            }
        }

        // Om det bara är plus och/eller minus
        if (!timesOrDivision)
        {
            // Loopa igenom alla objekt
            for (int currIndex = 0; currIndex < split.count(); currIndex++)
            {
                // Kolla ifall objektet är en operator
                if (operators.contains(split[currIndex]))
                {
                    // Isåfall vill vi beräkna termen före och efter
                    double term1 = split[currIndex-1].toDouble();
                    double term2 = split[currIndex+1].toDouble();

                    // Då vi vet att det bara är plus och minus:
                    if (split[currIndex] == "+")
                    {
                        answer = plus(term1, term2);
                    }
                    else if (split[currIndex] == "-")
                    {
                        answer = minus(term1, term2);
                    }

                    // Byt ut termen currIndex + 1 mot svaret
                    split.replace(currIndex+1,QString::number(answer));
                }
            }

            // Skriv ut nummret
            // Då write tar en QString som argument använder vi
            // QString::number för att konvertera nummret till en QString
            write(QString::number(answer));
            answerIsDisplayed = true;
        }
        // Om det finns multiplikation och/eller division
        else if (timesOrDivision)
        {
            int currIndex = 0;
            // Vi använder en while-loop för att kunna ändra currIndex
            // från insidan av loopen för att kunna ta hänsyn till
            // att vi tar bort vissa objekt efter att vi räknat ut dem
            while (currIndex < split.count())
            {
                // Om objektet i listan är en operator
                if (operators.contains(split[currIndex]))
                {
                    double term1 = split[currIndex-1].toDouble();
                    double term2 = split[currIndex+1].toDouble();

                    // Vi vet att det finns multipl. och/eller delat
                    // och kollar först efter dem då man alltid räknar
                    // multiplikation och division före plus och minus
                    if (split[currIndex] == "*")
                    {
                        answer = times(term1, term2);
                        // När vi tar bort currIndex kommer alla objekt
                        // efter currIndex flyttas ett steg åt vänster
                        // och få index == currIndex - 1
                        split.removeAt(currIndex);
                        split.removeAt(currIndex-1);
                        // När vi sedan tar bort objektet före kommer den
                        // ha samma index som innan (currIndex - 1)
                        //
                        // När vi sedan tar bort den kommer alla objekt till
                        // höger flyttas ett steg åt vänster igen och
                        // vi ersätter alltså currIndex - 1 med värdet på
                        // svaret. För att inte missa att räkna ut svaret
                        // måste vi gör currIndex 1 mindre (while-loopens index)
                        split.replace(currIndex-1, QString::number(answer));
                        currIndex -= 1;
                    }
                    else if (split[currIndex] == "/")
                    {
                        // Samma sak som ovan
                        answer = divided(term1, term2);
                        split.removeAt(currIndex);
                        split.removeAt(currIndex-1);
                        split.replace(currIndex-1, QString::number(answer));
                        currIndex -= 1;
                    }
                }

                // I slutet av varje iteration vill vi öka currIndex med 1
                currIndex++;
            }

            // När vi är klara med multiplikation och division
            // vill vi räkna ut alla plus och minus ifall de finns
            for (int currIndex2 = 0; currIndex2 < split.count(); currIndex2++)
            {
                // Samma som ovan
                if (!split[currIndex2].isEmpty())
                {
                    if (operators.contains(split[currIndex2]))
                    {
                        double term1 = split[currIndex2-1].toDouble();
                        double term2 = split[currIndex2+1].toDouble();

                        if (split[currIndex2] == "+")
                        {
                            answer = plus(term1, term2);
                        }
                        else if (split[currIndex2] == "-")
                        {
                            answer = minus(term1, term2);
                        }

                        split.replace(currIndex2+1,QString::number(answer));
                    }
                }
            }

            // Skriv ut svaret
            write(QString::number(answer));
            answerIsDisplayed = true;
        }

        // Återställ containsComma när uträkningen är klar så att
        // det går att skriva comma igen efteråt
        double temp;
        if (modf(answer, &temp) == 0)
        {
            containsComma = false;
        }
        else
        {
            containsComma = true;
        }
        // Efter en uträkning finns inga operatorer kvar
        hasOperator = false;
    }
}

void MainWindow::onEnterClicked()
{
    // Beräkna bara om det finns fler än en term
    if (hasOperator)
    {
        // Beräkna
        calculate();
    }
}
