#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QObject>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onButtonClicked();
    void onOperatorClicked();
    void onEnterClicked();
    void onClearClicked();
    void onCommaClicked();

private:
    Ui::MainWindow *ui;
    QString getCurrentOutputText();

    QString operators = "*/+-";
    double answer;
    bool answerIsDisplayed = false;
    bool hasOperator = false;
    bool containsComma = false;

    void write(QString text);
    QStringList getSplitOutput();

    double plus(double x, double y);
    double minus(double x, double y);
    double divided(double x, double y);
    double times(double x, double y);

    void calculate();
};

#endif // MAINWINDOW_H
