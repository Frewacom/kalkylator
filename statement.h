#ifndef STATEMENT_H
#define STATEMENT_H

#include <QObject>

class Statement
{

public:
    explicit Statement(QString data, QStringList operators);

    QString Data;
    QStringList Operators;

    QString Calculate();

private:
    QStringList split(QString data);

signals:

public slots:
};

#endif // STATEMENT_H
