/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *MainGrid;
    QTextEdit *Output;
    QPushButton *N_3;
    QPushButton *N_5;
    QPushButton *N_4;
    QPushButton *N_8;
    QPushButton *N_1;
    QPushButton *N_2;
    QPushButton *Divided;
    QPushButton *Times;
    QPushButton *N_0;
    QPushButton *Comma;
    QPushButton *Minus;
    QPushButton *Enter;
    QPushButton *Plus;
    QPushButton *N_9;
    QPushButton *N_6;
    QPushButton *N_7;
    QPushButton *Clear;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(250, 370);
        MainWindow->setMaximumSize(QSize(250, 370));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QLatin1String("QWidget\n"
"{\n"
"	background-color: rgb(46, 47, 48);\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"	width: 50px;\n"
"	height: 50px;\n"
"	background-color: #1b1b1b;\n"
"	color: white;\n"
"	font-size: 20px;\n"
"	border: none;\n"
"}\n"
"\n"
"QTextEdit\n"
"{\n"
"	height: 20px;\n"
"	background-color: #1b1b1b;\n"
"	color: white;\n"
"	border: none;\n"
"	font-size: 16px;\n"
"	padding: 10px 20px 10px 20px;\n"
"}"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        MainGrid = new QGridLayout();
        MainGrid->setSpacing(10);
        MainGrid->setObjectName(QStringLiteral("MainGrid"));
        Output = new QTextEdit(centralWidget);
        Output->setObjectName(QStringLiteral("Output"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Output->sizePolicy().hasHeightForWidth());
        Output->setSizePolicy(sizePolicy);
        Output->setMaximumSize(QSize(16777215, 50));
        Output->setReadOnly(true);

        MainGrid->addWidget(Output, 0, 0, 1, 4);

        N_3 = new QPushButton(centralWidget);
        N_3->setObjectName(QStringLiteral("N_3"));

        MainGrid->addWidget(N_3, 4, 2, 1, 1);

        N_5 = new QPushButton(centralWidget);
        N_5->setObjectName(QStringLiteral("N_5"));

        MainGrid->addWidget(N_5, 3, 1, 1, 1);

        N_4 = new QPushButton(centralWidget);
        N_4->setObjectName(QStringLiteral("N_4"));

        MainGrid->addWidget(N_4, 3, 0, 1, 1);

        N_8 = new QPushButton(centralWidget);
        N_8->setObjectName(QStringLiteral("N_8"));

        MainGrid->addWidget(N_8, 2, 1, 1, 1);

        N_1 = new QPushButton(centralWidget);
        N_1->setObjectName(QStringLiteral("N_1"));

        MainGrid->addWidget(N_1, 4, 0, 1, 1);

        N_2 = new QPushButton(centralWidget);
        N_2->setObjectName(QStringLiteral("N_2"));

        MainGrid->addWidget(N_2, 4, 1, 1, 1);

        Divided = new QPushButton(centralWidget);
        Divided->setObjectName(QStringLiteral("Divided"));

        MainGrid->addWidget(Divided, 2, 3, 1, 1);

        Times = new QPushButton(centralWidget);
        Times->setObjectName(QStringLiteral("Times"));

        MainGrid->addWidget(Times, 3, 3, 1, 1);

        N_0 = new QPushButton(centralWidget);
        N_0->setObjectName(QStringLiteral("N_0"));

        MainGrid->addWidget(N_0, 5, 0, 1, 1);

        Comma = new QPushButton(centralWidget);
        Comma->setObjectName(QStringLiteral("Comma"));

        MainGrid->addWidget(Comma, 5, 1, 1, 1);

        Minus = new QPushButton(centralWidget);
        Minus->setObjectName(QStringLiteral("Minus"));

        MainGrid->addWidget(Minus, 5, 3, 1, 1);

        Enter = new QPushButton(centralWidget);
        Enter->setObjectName(QStringLiteral("Enter"));

        MainGrid->addWidget(Enter, 5, 2, 1, 1);

        Plus = new QPushButton(centralWidget);
        Plus->setObjectName(QStringLiteral("Plus"));

        MainGrid->addWidget(Plus, 4, 3, 1, 1);

        N_9 = new QPushButton(centralWidget);
        N_9->setObjectName(QStringLiteral("N_9"));

        MainGrid->addWidget(N_9, 2, 2, 1, 1);

        N_6 = new QPushButton(centralWidget);
        N_6->setObjectName(QStringLiteral("N_6"));

        MainGrid->addWidget(N_6, 3, 2, 1, 1);

        N_7 = new QPushButton(centralWidget);
        N_7->setObjectName(QStringLiteral("N_7"));

        MainGrid->addWidget(N_7, 2, 0, 1, 1);

        Clear = new QPushButton(centralWidget);
        Clear->setObjectName(QStringLiteral("Clear"));

        MainGrid->addWidget(Clear, 1, 0, 1, 1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        MainGrid->addWidget(pushButton_2, 1, 1, 1, 1);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        MainGrid->addWidget(pushButton_3, 1, 2, 1, 1);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        MainGrid->addWidget(pushButton_4, 1, 3, 1, 1);

        MainGrid->setRowStretch(1, 5);
        MainGrid->setRowStretch(2, 5);
        MainGrid->setRowStretch(3, 5);
        MainGrid->setRowStretch(4, 5);

        gridLayout_2->addLayout(MainGrid, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        N_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        N_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        N_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        N_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        N_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        N_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        Divided->setText(QApplication::translate("MainWindow", "/", Q_NULLPTR));
        Times->setText(QApplication::translate("MainWindow", "*", Q_NULLPTR));
        N_0->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        Comma->setText(QApplication::translate("MainWindow", ",", Q_NULLPTR));
        Minus->setText(QApplication::translate("MainWindow", "-", Q_NULLPTR));
        Enter->setText(QApplication::translate("MainWindow", "=", Q_NULLPTR));
        Plus->setText(QApplication::translate("MainWindow", "+", Q_NULLPTR));
        N_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        N_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        N_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        Clear->setText(QApplication::translate("MainWindow", "C", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "XX", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MainWindow", "XX", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MainWindow", "XX", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
